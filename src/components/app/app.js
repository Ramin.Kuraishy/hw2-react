import { Component } from "react";
import "./app.scss";
import Button from "../Button/button";
import List from "../List/list";
import Modal from "../Modal/modal";
import Header from "../Header/header";
import propTypes from "prop-types";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal1: false,
      like: [],
      cart: [],
      readyToAddId: null,
    };
  }

  addToCart = () => {
    this.setState(
      () => ({
        cart: [...this.state.cart, this.state.readyToAddId],
        modal1: false,
      }),
      () => {
        localStorage.setItem("cart", JSON.stringify(this.state.cart));
      }
    );
  };
  setReadyToAddId = id => {
    this.setState(() => ({
      readyToAddId: id,
    }));
  };

  showAddModal = () => {
    this.setState(() => ({
      modal1: true,
    }));
  };
  closeAllModal = () => {
    this.setState(() => ({
      modal1: false,
    }));
  };
  toggleLike = product => {
    let flag = false;
    this.state.like.forEach(element => {
      if (element.article === product.article) {
        flag = true;
      }
    });
    if (flag) {
      this.setState(
        () => ({
          like: this.state.like.filter(
            item => item.article !== product.article
          ),
        }),
        () => {
          localStorage.setItem("like", JSON.stringify(this.state.like));
        }
      );
    } else {
      this.setState(
        () => ({
          like: [...this.state.like, product],
        }),
        () => {
          localStorage.setItem("like", JSON.stringify(this.state.like));
        }
      );
    }
  };
  render() {
    const { like, cart } = this.state;
    console.log(like);
    return (
      <div className="App">
        <Header like={like.length} cart={cart.length}/>

        {this.state.modal1 && (
          <Modal
            isClose={this.closeAllModal}
            text="Are you sure that you want to add this item?"
            header="Add"
            closeButton={true}
            actions={
              <>
                <Button
                  backgroundColor={"#383737"}
                  name="Ok"
                  clickHandler={this.addToCart}
                />
                <Button
                  backgroundColor={"#383737"}
                  name="Cancel"
                  clickHandler={this.closeAllModal}
                />
              </>
            }
          />
        )}

        <List
          showModal={this.showAddModal}
          favlike={this.toggleLike}
          setReadyToAddId={this.setReadyToAddId}
        />
      </div>
    );
  }
}

export default App;
