import { Component } from "react";
import Button from "../../Button/button";
import "./listItem.scss";
import propTypes from "prop-types"

class ListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      heart: false,
    };
  }

  addFavAndChangeColor = item => {
    this.props.favlike(item);
    this.setState(() => ({
      heart: !this.state.heart,
    }));
  };
  addToCart = () => {
    this.props.setReadyToAddId(this.props.item);
    this.props.showModal();
  };

  render() {
    const { item } = this.props;
    const { heart } = this.state;
    return (
      <div style={{ width: "18rem" }}>
        <img className="card-img-top" src={item.url} alt="" />
        <div className="card-body">
          <li>{item.name}</li>
          <li>{item.color}</li>
          <li>{item.article}</li>
          <li>{item.price}</li>
        </div>
        <div className="card-btn">
          <Button
            name="Add"
            backgroundColor="#383737"
            clickHandler={this.addToCart}
          />
          <i
            onClick={() => this.addFavAndChangeColor(item)}
            style={
              heart
                ? { color: "red", marginLeft: "10px", fontSize: "1.5em" }
                : { color: "black", marginLeft: "10px", fontSize: "1.5em" }
            }
            className={"fas fa-light fa-heart"}
          ></i>
        </div>
      </div>
    );
  }
}
ListItem.propTypes ={
  item: propTypes.object
}
ListItem.defaultProps = {
  
}
export default ListItem;
