import React from "react";
import { Component } from "react";
import ListItem from "./ListItem/listItem";
import "./list.scss";
import propTypes from "prop-types"

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
    };
  }

  componentDidMount() {
    fetch("./data.json")
      .then(res => res.json())
      .then(result => this.setState(state => (state.items = result)));
  }

  render() {
    const { items } = this.state;
    const { setReadyToAddId, showModal, favlike } = this.props
    return (
      <ul className="page-list">
        {items.map(item => (
          <ListItem
            key={item.article.toString()}
            item={item}
            items={items}
            setReadyToAddId={setReadyToAddId}
            showModal={showModal}
            favlike={favlike}
          />
        ))}
      </ul>
    );
  }
}
List.propTypes = {
setReadyToAddId: propTypes.func,
showModal: propTypes.func,
favlike: propTypes.func,
}
List.defaultProps = {
  
}
export default List;
