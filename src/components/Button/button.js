import { Component } from "react";
import "./button.scss";
import propTypes from "prop-types"

class Button extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {backgroundColor, clickHandler, name} = this.props
    return (
      <div>
        <button
          type="button"
          className="btn btn-primary"
          style={{ backgroundColor: backgroundColor }}
          onClick={clickHandler}
        >
          {name}
        </button>
      </div>
    );
  }
}
Button.propTypes = {
name: propTypes.string,
backgroundColor: propTypes.string,
clickHandler: propTypes.func
}
Button.defaultProps = {
  name: 'Ok'
}
export default Button;
