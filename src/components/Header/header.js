import { Component } from "react";
import "./header.scss";
import propTypes from "prop-types";

class Header extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { like, cart } = this.props;
    return (
      <header className="page-header">
        <div className="header-nav">
          <i className="logo fa-brands fa-amazon"></i>
          <div className="header-icons">
            <i className="cart fas fa-cart-plus"></i>
            <p>{cart}</p>
            <i className="like fas fa-heart"></i>
            <p>{like}</p>
          </div>
        </div>
      </header>
    );
  }
}
Header.propTypes = {
  cart: propTypes.number,
  like: propTypes.number,
};
Header.defaultProps = {
  cart: 0,
  like: 0,
};
export default Header;
