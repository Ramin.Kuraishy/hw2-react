import React from "react";
import { Component } from "react";
import "./modal.scss";
import propTypes from "prop-types"
class Modal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { closeButton, text, header, isClose, actions } = this.props;

    return (
      <div className="dark" onClick={isClose}>
        <div className="modal" tabIndex="-1">
          <div
            className="modal-dialog"
            onClick={event => event.stopPropagation()}
          >
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">{header}</h5>
                {closeButton && (
                  <button
                    onClick={isClose}
                    type="button"
                    className="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                )}
              </div>
              <div className="modal-body">
                <p>{text}</p>
              </div>
              <div className="modal-footer">{actions}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
Modal.propTypes = {
  closeButton: propTypes.bool, 
  text: propTypes.string, 
  header: propTypes.string, 
  isClose: propTypes.func, 
  actions: propTypes.object
}
Modal.defaultProps = {

}
export default Modal;
